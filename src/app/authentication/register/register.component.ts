import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  registerForm : FormGroup;
  constructor(private formBuilder: FormBuilder,) { }
  submitted : boolean;

  ngOnInit(){
    this.submitted = false;
    this.registerForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    })
  }

  get f() { return this.registerForm.controls; }
 
  onSubmit() {
    this.submitted = true;
    if (this.registerForm.invalid) {
      return;
  } else {
    //add user data to localStorage / backend
    this.registerForm.reset();
  }

}
}
