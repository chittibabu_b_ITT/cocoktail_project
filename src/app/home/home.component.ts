import { ThrowStmt } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { DrinksService } from '../services/drinks.service'
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})



export class HomeComponent implements OnInit {
  drinkData = {
    name: "",
    imgurl: "",
    category: "",
    alcoholic: "",
    glass: "",
    instructions: "",
    instructionsDE: "",
    ingredient1: "",
    ingredient2: "",
    ingredient3: "",
    ingredient4: "",
    measure1: "",
    measure2: "",
    measure3: "",
    measure4: "",
  };

  constructor(private drink: DrinksService,) { }

  ngOnInit(): void {
    this.setRandomDrink();
  }

  setRandomDrink() {
    this.drink.getRandomDrink().subscribe(data => {
      this.drinkData.name = data['drinks'][0].strDrink;
      this.drinkData.imgurl = data['drinks'][0].strDrinkThumb;
      this.drinkData.category = data['drinks'][0].strCategory;
      this.drinkData.alcoholic = data['drinks'][0].strAlcoholic;
      this.drinkData.glass = data['drinks'][0].strGlass;
      this.drinkData.instructions = data['drinks'][0].strInstructions;
      this.drinkData.instructionsDE = data['drinks'][0].strInstructionsDE;
      this.drinkData.ingredient1 = data['drinks'][0].strIngredient1;
      this.drinkData.ingredient2 = data['drinks'][0].strIngredient2;
      this.drinkData.ingredient3 = data['drinks'][0].strIngredient3;
      this.drinkData.ingredient4 = data['drinks'][0].strIngredient4;
      this.drinkData.measure1 = data['drinks'][0].strMeasure1;
      this.drinkData.measure2 = data['drinks'][0].strMeasure2;
      this.drinkData.measure3 = data['drinks'][0].strMeasure3;
      this.drinkData.measure4 = data['drinks'][0].strMeasure4;


    })
  }

}