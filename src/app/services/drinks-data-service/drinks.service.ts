import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
@Injectable({
  providedIn: 'root'
})
export class DrinksService {

  constructor(private http: HttpClient) { }


  getRandomDrink() {
    let randomDrink = "https://www.thecocktaildb.com/api/json/v1/1/random.php";
    return this.http.get(randomDrink);
  }

  getDrinksList(type: string) {
    let drinksList: string;
    if (type == "category") {
      drinksList = "https://www.thecocktaildb.com/api/json/v1/1/list.php?c=list";
    }
    else if (type == "ingredients") {
      drinksList = "https://www.thecocktaildb.com/api/json/v1/1/list.php?i=list";
    }
    else if (type == "glass") {
      drinksList = "https://www.thecocktaildb.com/api/json/v1/1/list.php?g=list";
    }
    else if (type == "alcohol") {
      drinksList = "https://www.thecocktaildb.com/api/json/v1/1/list.php?a=list";
    }
    return this.http.get(drinksList);
  }

  getDrinksListFiltered(type: string, value: string) {
    let drinksList: string;
    if (type == "category") {
      drinksList = "https://www.thecocktaildb.com/api/json/v1/1/filter.php?c=" + value;
    }
    else if (type == "ingredient") {
      drinksList = "https://www.thecocktaildb.com/api/json/v1/1/filter.php?i=" + value;
    }
    else if (type == "glass") {
      drinksList = "https://www.thecocktaildb.com/api/json/v1/1/filter.php?g=" + value;
    }
    else if (type == "alcohol") {
      drinksList = "https://www.thecocktaildb.com/api/json/v1/1/filter.php?a=" + value;
    }
    return this.http.get(drinksList);
  }

  getDrinksListBySearch(value: string) {
    let drinksList = "https://www.thecocktaildb.com/api/json/v1/1/search.php?s=" + value;
    return this.http.get(drinksList);
  }

  getDrinkDetailsById(drinkId: string) {
    let drinksList = "https://www.thecocktaildb.com/api/json/v1/1/lookup.php?i=" + drinkId;
    return this.http.get(drinksList);
  }

}
