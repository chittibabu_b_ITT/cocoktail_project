import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './authentication/login/login.component';
import { RegisterComponent } from './authentication/register/register.component';
import { CocktailsListComponent } from './cocktails-list/cocktails-list.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'cocktails-list',
    component: CocktailsListComponent
  },
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  {
     path: 'auth', 
     loadChildren: () => 
     import('./authentication/authentication.module').then(m => m.AuthenticationModule) 
    },
  {
    path: 'login',
    component: LoginComponent
  }, {
    path: '**',
    component: RegisterComponent
  },
  {
    path: '**',
    redirectTo: '/home',
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
