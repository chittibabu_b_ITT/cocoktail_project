import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DrinksService } from '../services/drinks.service';

@Component({
  selector: 'app-cocktails-list',
  templateUrl: './cocktails-list.component.html',
  styleUrls: ['./cocktails-list.component.scss']
})
export class CocktailsListComponent implements OnInit {
  category: any;
  query: string;
  drinkCards = [];
  cardDetails: any;
  isLoadingMain: boolean;
  hasNoData: boolean;

  constructor(private route: ActivatedRoute, private drink: DrinksService,) { }

  ngOnInit(): void {
    this.isLoadingMain = true;
    this.hasNoData = false;
    this.route.queryParams.subscribe(param => {
      this.category = param.filterType;
      this.query = param.value;
      if (this.category != "search") {
        this.getDrinksByCategory(this.category, this.query);
      } else {
        this.getDrinksBySearch(this.query);
      }
    });

  }

  getDrinksByCategory(category: string, value: string) {
    this.drinkCards = [];
    category.replace(" ", "_")
    this.drink.getDrinksListFiltered(category, value).subscribe(data => {
      this.isLoadingMain = false;
      for (let i = 0; i < data['drinks'].length; i++) {
        this.drinkCards.push(data['drinks'][i]);
      }

    })
  }

  getDrinksBySearch(value: string) {
    this.drinkCards = [];
    this.drink.getDrinksListBySearch(value).subscribe(data => {
      if (data['drinks'] !== null) {
        this.isLoadingMain = false;
        this.hasNoData = false;
        for (let i = 0; i < data['drinks'].length; i++) {
          this.drinkCards.push(data['drinks'][i]);
        }
      } else {
        this.isLoadingMain = false;
        this.hasNoData = true;
      }

    })
  }

  showDrinkDetails(drinkid: string) {
    document.getElementById(drinkid + "-loader").classList.toggle("hidden");
    this.cardDetails = {};
    this.drink.getDrinkDetailsById(drinkid).subscribe(data => {
      console.log(data)
      this.cardDetails = data['drinks'][0];
      if (data['drinks'] !== null) {
        document.getElementById(drinkid + "-category").innerHTML = "<b>Category: </b>" + this.cardDetails.strCategory;
        document.getElementById(drinkid + "-alocholic").innerHTML = "<b>Alcoholic: </b>" + this.cardDetails.strAlcoholic;
        document.getElementById(drinkid + "-glass").innerHTML = "<b>Glass: </b>" + this.cardDetails.strGlass;
        document.getElementById(drinkid + "-instructions").innerHTML = "<b>Instructions: </b>" + this.cardDetails.strInstructions;
        document.getElementById(drinkid + "-instructionsDE").innerHTML = "<b>InstructionsDE: </b>" + this.cardDetails.strInstructionsDE;
        document.getElementById(drinkid + "-ingredients").innerHTML = this.cardDetails.strIngredient1 + " , " + this.cardDetails.strIngredient2 + " , " + this.cardDetails.strIngredient3 + " , " + this.cardDetails.strIngredient4;
        document.getElementById(drinkid + "-measure").innerHTML = this.cardDetails.strMeasure1 + " , " + this.cardDetails.strMeasure2 + " , " + this.cardDetails.strMeasure3 + " , " + this.cardDetails.strMeasure4;
        document.getElementById(drinkid).classList.toggle("hidden");
        document.getElementById(drinkid + "-loader").classList.toggle("hidden");

      } else {
        console.log("Results NOT Found");
      }
    })
  }

}
