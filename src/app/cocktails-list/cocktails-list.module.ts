import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CocktailsListRoutingModule } from './cocktails-list-routing.module';
import { CocktailsListComponent } from './cocktails-list.component';


@NgModule({
  declarations: [CocktailsListComponent],
  imports: [
    CommonModule,
    CocktailsListRoutingModule
  ]
})
export class CocktailsListModule { }
