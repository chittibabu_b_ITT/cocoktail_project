import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CocktailsListComponent } from './cocktails-list.component';

const routes: Routes = [{ path: '', component: CocktailsListComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CocktailsListRoutingModule { }
