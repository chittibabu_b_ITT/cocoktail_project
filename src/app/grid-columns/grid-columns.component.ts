import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-grid-columns',
  templateUrl: './grid-columns.component.html',
  styleUrls: ['./grid-columns.component.scss']
})
export class GridColumnsComponent implements OnInit {


  cardsData = [];

  constructor() {
    this.cardsData = [
      {
        name : "abc",
        description : "csdas",
      },
      {
        name : "abc",
        description : "adsasadas"
      },
      {
        name : "abc",
        description : "csdascsdascsdascsdas"
      },
      {
        name : "abc",
        description : "csdas",
      },
      {
        name : "abc",
        description : "Lorem ipsum dolor sit amet consectetur adipisicing elit. Rerum atque cumque id ullam aut iste nemo! Iure officiis, sunt odio neque quae error nobis iste esse tenetur, tempora earum quam?"
      },
      {
        name : "abc",
        description : "csdascsdascsdascsdas"
      },{
        name : "abc",
        description : "csdas",
      },
      {
        name : "abc",
        description : "adsasadas"
      },
      {
        name : "abc",
        description : "csdascsdascsdascsdas"
      },{
        name : "abc",
        description : "csdas",
      },
      {
        name : "abc",
        description : "adsasadas"
      },
      {
        name : "abc",
        description : "csdascsdascsdascsdas"
      },{
        name : "abc",
        description : "csdas",
      },
      {
        name : "abc",
        description : "adsasadas"
      },
      {
        name : "abc",
        description : "csdascsdascsdascsdas"
      },
    ]
   }


  ngOnInit(): void {
  }


}
