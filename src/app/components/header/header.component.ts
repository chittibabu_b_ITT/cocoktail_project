import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DrinksService } from '../../services/drinks-data-service/drinks.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})



export class HeaderComponent implements OnInit {

  categoryList = [];
  glassTypeList = [];
  ingredientsList = [];
  alcoholList = [];

  constructor(private router: Router, private drink: DrinksService,) { }

  ngOnInit(): void {
    const form = document.querySelector('.form-inline') as HTMLFormElement;
    const inputValue = document.querySelector('#searchInput') as HTMLInputElement;
    this.setDrinksLists();

    form.addEventListener('submit', (e: Event) => {
      e.preventDefault();
      this.router.navigate(['/cocktails-list'],
        {
          queryParams:
          {
            filterType: 'search',
            value: inputValue.value,
          }
        });
    })

  }

  setDrinksLists() {

    this.drink.getDrinksList("category").subscribe(data => {
      for (let i = 0; i < data['drinks'].length; i++) {
        this.categoryList.push(data['drinks'][i]);
      }

    })
    this.drink.getDrinksList("ingredients").subscribe(data => {
      for (let i = 0; i < data['drinks'].length; i++) {
        this.ingredientsList.push(data['drinks'][i]);
      }
    })
    this.drink.getDrinksList("glass").subscribe(data => {
      for (let i = 0; i < data['drinks'].length; i++) {
        this.glassTypeList.push(data['drinks'][i]);
      }
    })
    this.drink.getDrinksList("alcohol").subscribe(data => {
      for (let i = 0; i < data['drinks'].length; i++) {
        this.alcoholList.push(data['drinks'][i]);
      }
    })
  }

  gotoCockTailsList(value: any, category: string) {
    this.router.navigate(['/cocktails-list'],
      {
        queryParams:
        {
          filterType: category,
          value: value,
        }
      });
  }

}
